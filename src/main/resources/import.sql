-- You can use this file to load seed data into the database using SQL statements
-- Categories parents
insert into Category (uuid, name, description, priority, version) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b001', 'Calles', 'Calzada y veredas', 'LOW', 1)
insert into Category (uuid, name, description, priority, version) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b002', 'Plazas', 'Plazas, parques y otras zonas verdes', 'LOW', 1)
insert into Category (uuid, name, description, priority, version) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b003', 'Limpieza', 'Limpieza pública, higiene y acumulación de basura', 'LOW', 1)
insert into Category (uuid, name, description, priority, version) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b004', 'Denuncias', 'Peligros e irregularidades', 'LOW', 1)
insert into Category (uuid, name, description, priority, version) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b005', 'Otros', 'Seleccione este si no coincide en los anteriores', 'LOW', 1)

-- Calles
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b101', 'Baches', 'Baches en la calzada.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b102', 'Iluminación', 'Falta de iluminación, o que no se apaga.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b103', 'Semáforos y señalización', 'Semáforos rotos, falta de cartel de señalización, etc.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b104', 'Veredas', 'Veredas rotas, intransitables, etc.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b105', 'Vandalismo', 'Daños en inmobiliario urbano.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b106', 'Otros', 'Puede cargar aquí si no coincide con las anteriores categorías.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b001')

-- Plazas
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b111', 'Juegos', 'Juegos mecánicos rotos, en mal estado.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b002')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b112', 'Iluminación', 'Falta de iluminación, o que no se apaga.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b002')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b113', 'Otros', 'Puede cargar aquí si no coincide con las anteriores categorías.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b002')

-- Limpieza
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b121', 'Basura', 'Basura acumulada, sin recoger, etc.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b122', 'Escombros', 'Escombros en vía pública.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b123', 'Cloacas', 'Desbordes de cloacas.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b124', 'Desagues tapados', 'Desagues sucios, tapados, etc.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b125', 'Pastizales', 'Pastizales altos.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b126', 'Otros', 'Puede cargar aquí si no coincide con las anteriores categorías.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b003')

-- Denuncias
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b131', 'Cartelería peligrosa', 'Carteles mal colocados, con riesgos eléctricos, etc.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b004')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b132', 'Falta de habilitación', 'Falta de habilitación municipal', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b004')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b133', 'Obra en construcción', 'Obra en construcción irregular.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b004')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b134', 'Cables eléctricos peligrosos', 'Cables eléctricos peligrosos.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b004')
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b135', 'Otros', 'Puede cargar aquí si no coincide con las anteriores categorías.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b004')

-- Otros
insert into Category (uuid, name, description, priority, version, category_group_id) values ('a8a70dc4-a87d-48e8-a197-1ed380c8b141', 'Otros', 'Puede cargar aquí si no coincide con las anteriores categorías.', 'LOW', 1, 'a8a70dc4-a87d-48e8-a197-1ed380c8b005')