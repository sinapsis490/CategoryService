package ar.com.leadprogrammer.framework;

import javax.annotation.PostConstruct;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ar.com.leadprogrammer.cityreport.rest.CategoryResource;
import ar.com.leadprogrammer.cityreport.rest.HealthCheck;
import ar.com.leadprogrammer.framework.configuration.MicroserviceStartUp.JerseyConfig;

@Configuration
@ComponentScan(value="ar.com.leadprogrammer.cityreport", includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Provider.class))
@EntityScan( basePackages={"ar.com.leadprogrammer.cityreport.model.entities"})
@EnableJpaRepositories(basePackages = {"ar.com.leadprogrammer.cityreport.model.service"})
@EnableTransactionManagement
public class Application {

	@Autowired
	JerseyConfig jerseyConfig;
	
	@PostConstruct
	public void initJersey() {
		jerseyConfig.register(HealthCheck.class);
		jerseyConfig.register(CategoryResource.class);

	}
	

}