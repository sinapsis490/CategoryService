package ar.com.leadprogrammer.cityreport.rest;

import ar.com.leadprogrammer.cityreport.rest.model.category.ExternalCategory;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.process.internal.RequestScoped;

import java.util.List;

/**
 * Created by erodrig on 04/09/14.
 */

@Path("/")
@RequestScoped
@Produces({ MediaType.TEXT_PLAIN })
@Consumes({ MediaType.TEXT_PLAIN })
public class HealthCheck {

    @GET
    public Response checkServerStatus() {

        return Response.ok().build();

    }


}
