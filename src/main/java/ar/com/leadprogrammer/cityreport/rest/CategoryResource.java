package ar.com.leadprogrammer.cityreport.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.leadprogrammer.cityreport.model.entities.Category;
import ar.com.leadprogrammer.cityreport.model.service.CategoryAdministrationService;
import ar.com.leadprogrammer.cityreport.rest.model.category.ExternalCategory;


@Path("/category")
@Component
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class CategoryResource {
	
	@Autowired
	protected CategoryAdministrationService categoryAdministrationService;

	@GET
    @RolesAllowed({"administrator"})
	public Response listAllCategories() {
		
		List<ExternalCategory> retorno =categoryAdministrationService.queryFindAll();

		return Response.ok().entity(new MakeCategoriesRootNode(retorno)).build();

	}

	@POST
    @RolesAllowed({"administrator"})
	public Response createCategory(ExternalCategory external) {
		
		Category category = new Category(external);
		try {
			categoryAdministrationService.saveCategory(category);
		} catch (Exception e) {
			e.printStackTrace();
	        return Response.serverError().build();

		}
		
        return Response.ok().build();

	}
	
	
	class MakeCategoriesRootNode{
		List<ExternalCategory> category;

		public MakeCategoriesRootNode(List<ExternalCategory> retorno) {
			this.category = retorno;
		}

		public List<ExternalCategory> getCategories() {
			return category;
		}

		public void setCategories(List<ExternalCategory> categories) {
			this.category = categories;
		}
		
		
		
	}


}


