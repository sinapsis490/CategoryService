package ar.com.leadprogrammer.cityreport.rest.model.category;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.leadprogrammer.cityreport.model.entities.Category;


@XmlRootElement(name="category")
public class ExternalCategory {
	
	private String id;
    

	@NotNull
	private String name;
    private String description;
	private String priority;
	private ExternalAssociatedCategory categorygroup;

	public ExternalCategory() {
	}

	public ExternalCategory(Category currentCategory) {
		this.setId(currentCategory.getUuid()!=null ? currentCategory.getUuid().toString() : null);
		this.setName(currentCategory.getName());
		this.setDescription(currentCategory.getDescription());
		this.setPriority(currentCategory.getPriority().toString());
		Category group = currentCategory.getCategoryGroup();
		this.setCategorygroup(group !=null ? new ExternalAssociatedCategory(group.getUuid().toString(), group.getName()) : null);
		
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public ExternalAssociatedCategory getCategorygroup() {
		return categorygroup;
	}
	public void setCategorygroup(ExternalAssociatedCategory categorygroup) {
		this.categorygroup = categorygroup;
	}



	
	
}
