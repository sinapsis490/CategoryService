package ar.com.leadprogrammer.cityreport.rest.model.category;



public class ExternalAssociatedCategory {
	private String id;
	private String name;
	
	
	public ExternalAssociatedCategory() {
		super();
	}
	public ExternalAssociatedCategory(String uuid, String name) {
		this.id = uuid;
		this.name= name;
		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
