package ar.com.leadprogrammer.cityreport.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import ar.com.leadprogrammer.cityreport.rest.model.category.ExternalCategory;

@Entity
@Table(name = "Category")
public class Category extends BaseEntity {

	@NotNull
	@NotEmpty
	private String name;

	private String description;

	@ManyToOne
    @JoinColumn(name = "category_group_id")
	private Category categoryGroup;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "categoryGroup", fetch = FetchType.EAGER)
	private List<Category> child;

	@Enumerated(EnumType.STRING)
	private Priority priority;

	public Category() {
	}

	public Category(ExternalCategory external) {
		if (external.getId() != null) {
			
			this.setUuid(external.getId().toString());

		}
		this.setDescription(external.getDescription());
		this.setName(external.getName());
		this.setPriority(external.getPriority() != null ? Priority
				.valueOf(external.getPriority()) : null);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategoryGroup() {
		return categoryGroup;
	}

	public void setCategoryGroup(Category father) {
		this.categoryGroup = father;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

}
