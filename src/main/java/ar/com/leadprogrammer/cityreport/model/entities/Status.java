package ar.com.leadprogrammer.cityreport.model.entities;

public enum Status {

	NEW, TAKEN, SOLVED, ON_HOLD, DISCARDED
}
