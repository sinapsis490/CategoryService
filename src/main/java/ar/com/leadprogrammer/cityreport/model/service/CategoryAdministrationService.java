package ar.com.leadprogrammer.cityreport.model.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.leadprogrammer.cityreport.model.entities.Category;
//import ar.com.leadprogrammer.cityreport.model.entities.Category_;
import ar.com.leadprogrammer.cityreport.rest.model.category.ExternalCategory;

@Stateless
@Repository
@Component
@Transactional
public class CategoryAdministrationService{

	private static final Logger log = Logger
			.getLogger(CategoryAdministrationService.class.getName());

	@PersistenceContext
	protected EntityManager em;

	
	public Category findByUuid(String categoryId) {
		return em.find(Category.class, categoryId);
	}

	
	public List<ExternalCategory> queryFindAll() {
		List<ExternalCategory> result = new ArrayList<>();

		String query = "select u from Category u";
		List<Category> internalUsers = em.createQuery(query).getResultList();

		for (Category currentCategory : internalUsers) {

			result.add(new ExternalCategory(currentCategory));
		}
		
		return result;
	}

	
	
	public void saveCategory(Category category) throws Exception {

		try {
			log.info("Entering saveMember");

			em.persist(category);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
	}


	public Category findByName(String name) {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> query = cb.createQuery(Category.class);
		Root<Category> category = query.from(Category.class);
		ParameterExpression<String> nameP = cb.parameter(String.class);
		//query.select(category).where(cb.equal(category.get(Category_.name), nameP));

		return em.createQuery(query).setParameter(nameP, name).getSingleResult();
		
	}

}
