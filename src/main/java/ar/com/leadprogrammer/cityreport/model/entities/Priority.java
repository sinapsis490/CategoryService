package ar.com.leadprogrammer.cityreport.model.entities;

public enum Priority {
	
	UNASSIGNED, LOW, MEDIUM , HIGH, CRITICAL

}
